import webapp

PAGE = """
<!DOCTYPE html>
<html lang="es">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="es">
  <body>
    <p>Recurso no encontrado: {resource}.</p>
  </body>
</html>
"""


class ContentApp(webapp.webApp):

    def pag_principal(self):
        return "<p>Pagina principal, bienvenido!</p>"

    def hola(self):
        return "<p>Hola mundo!</p>"

    def adios(self):
        return "<p>Adios mundo!</p>"

    def no_encontrado(self):
        return PAGE_NOT_FOUND.format(resource=self.resource)

    def parse(self, request):
        return request.split(' ', 2)[1]

    def process(self, resource):
        self.resource = resource
        handler_name = 'no_encontrado'
        if resource == '/':
            handler_name = 'pag_principal'
        elif resource == '/hola':
            handler_name = 'hola'
        elif resource == '/adios':
            handler_name = 'adios'

        handler = getattr(self, handler_name)
        content = handler()
        page = PAGE.format(content=content)

        if handler_name != 'not_found':
            code = "200 OK"
        else:
            code = "404 Not Found"

        return code, page


if __name__ == "__main__":
    webApp = ContentApp("localhost", 1234)
